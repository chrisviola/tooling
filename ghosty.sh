#!/bin/bash

usage() {
    local SCRIPT="$(basename -- "$0")"

    echo -e "$SCRIPT - generate sosmap commands for quote account enabling"
    echo -e "and ghost mode user creation and enabling"
    echo -e "usage:\t$SCRIPT <account> <role> <vatid>"
    echo -e "where"
    echo -e "\t<account> is the person's e-mail address"
    echo -e "\t<role> is either \"buyer\" or \"vendor\""
    echo -e "\t<vatid> is the Umsatzsteuer-ID, e. g. DE000000001"
}

if [[ $# -ne 3 ]] ; then
    usage
    
    exit 0
fi

PERSON=$1
ROLE=$2
VATID=$3

cat <<EOS
# enable account to use quote
addScopeToUser $PERSON app:quote:${ROLE}
addAppToUser $PERSON xom-quote

# create ghost account with same organization and enable it
addUser quote+${ROLE}-${VATID}@xom-materials.com $(pwgen -s 32 1) ${VATID} quote+${ROLE}-${VATID}@xom-materials.com
changeUserMail quote+${ROLE}-${VATID}@xom-materials.com quote+${ROLE}-${VATID}@xom-materials.com

addScopeToUser quote+${ROLE}-${VATID}@xom-materials.com app:quote:${ROLE}
addAppToUser quote+${ROLE}-${VATID}@xom-materials.com xom-quote
EOS

